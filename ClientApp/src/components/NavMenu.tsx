import * as React from "react";
import {
  Collapse,
  Container,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";
import "./NavMenu.css";

export default class NavMenu extends React.PureComponent<
  {},
  { isOpen: boolean }
> {
  public state = {
    isOpen: false,
  };

  public render() {
    return (
      <header>
        <Navbar
          className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3"
          light
        >
          <Container>
            <NavbarBrand tag={Link} to="/"></NavbarBrand>
            <NavbarToggler onClick={this.toggle} className="mr-2" />
            <Collapse
              className="d-sm-inline-flex flex-sm-row-reverse"
              isOpen={this.state.isOpen}
              navbar
            >
              <ul className="navbar-nav flex-grow">
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/profile">
                    Profile
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/rangList">
                    Rang lista
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/tasks">
                    Zadaci za grupu
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/appointment">
                    Zahtevaj termin
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/rules">
                    Pravila programa
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/forum">
                    Forum
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/recepies">
                    Recepti
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} className="nav" to="/groupchat">
                    Grupni chat
                  </NavLink>
                </NavItem>
              </ul>
            </Collapse>
          </Container>
        </Navbar>
      </header>
    );
  }

  private toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };
}
