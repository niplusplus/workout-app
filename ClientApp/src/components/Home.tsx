import * as React from "react";
import { connect } from "react-redux";
import { Link, RouteComponentProps } from "react-router-dom";
import {
  Collapse,
  Container,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
} from "reactstrap";
import Axios from "axios";

interface IInProgress {
  startingDate: any;
}

interface ComponentProps extends RouteComponentProps<IInProgress> {}

class Home extends React.PureComponent<ComponentProps> {
  state = {
    startingDate: "",
    day: "",
  };

  componentDidMount() {
    let ceodatum, day, month, year, time;
    Axios.get("http://localhost:5000/api/Programs/next").then((response) => {
      this.setState({
        startingDate: response.data["startingDate"],

        //day: response.data["startingDate"].slice(0, 2),
      });
      console.log(this.state.startingDate);
      if (this.state.startingDate != undefined) {
        ceodatum = this.state.startingDate;
        day = ceodatum.slice(0, 2);
        month = ceodatum.slice(3, 6);
        year = ceodatum.slice(7, 10);
        time = ceodatum.slice(10, 18);
      } else {
        ceodatum = "01 Oct 2020 10:00:00";
        day = ceodatum.slice(0, 2);
        month = ceodatum.slice(3, 6);
        year = ceodatum.slice(7, 10);
        time = ceodatum.slice(10, 18);
      }

      console.log(day);
      console.log(month);
      console.log(year);
      console.log(time);
      //console.log(this.state.startingDate);

      let noviDatum = month + " " + day + " " + year + " " + time;
      console.log(noviDatum);
      let countDownDate = new Date(noviDatum).getTime();
      //console.log(new Date("11 10 2022 10:00:00"));
      // Update the count down every 1 second
      let x = setInterval(function () {
        // Get today's date and time
        let now = new Date().getTime();

        // Find the distance between now and the count down date
        let distance = countDownDate - now;
        console.log(countDownDate);
        console.log(now);
        // Time calculations for days, hours, minutes and seconds
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        let countdown = document.getElementById(
          "countdown"
        ) as HTMLParagraphElement;

        let countdowntext = document.getElementById(
          "countdown-text"
        ) as HTMLParagraphElement;

        // Output the result in an element with id="demo"
        if (countdown) {
          countdown.innerHTML =
            days + " : " + hours + " : " + minutes + "  : " + seconds + " ";
        }

        // If the count down is over, write some text
        console.log(distance);
        if (distance < 0) {
          //clearInterval(x);
          if (countdown) {
            countdown.innerHTML = "Izazov je u toku";
          }
          if (countdowntext) countdowntext.style.display = "none";
        }
      }, 100);
    });
    // Set the date we're counting down to
    console.log(this.state.startingDate);
    //let countDownDate = new Date("Oct 15 2022 10:00:00").getTime();
  }

  public render() {
    return (
      <React.Fragment>
        <div>
          <div className="first-screen">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-4"></div>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                  <p id="countdown-text" className="intro">
                    Izazov počinje za:
                  </p>
                  <p id="countdown" className="intro"></p>
                  <section className="intro">
                    <button
                      id="js-trigger-overlay"
                      className="intro"
                      type="button"
                    >
                      <NavLink tag={Link} to="/questionnaire">
                        Prijavi se
                      </NavLink>
                    </button>
                  </section>
                  <br />
                  <section className="intro">
                    <button
                      id="js-trigger-overlay"
                      className="intro"
                      type="button"
                    >
                      <NavLink tag={Link} to="/login">
                        Profil
                      </NavLink>
                    </button>
                  </section>
                </div>
              </div>
            </div>
          </div>
          <div className="scroll-down"> </div>
          <div className="second-screen">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-6">
                  <br />
                  <br />
                  <h1 className="intro text">Challenge</h1>
                  <br />
                  <br />
                  <p className="intro text">
                    Hvala Vam na interesovanju za Max challenge. Raduje nas što
                    je više od 900 ljudi na ovaj način unapredilo svoj život i
                    izgubilo suvišne kilograme, steklo nove pravilne navike u
                    ishrani, postalo fizički aktivno, zadovoljno sobom i
                    samouvereno.
                  </p>
                  <p className="intro text">
                    Najčešći razlozi zbog kojih ljudi učestvuju u Max challengu
                    su: želja da smršaju, da prestanu da jedu slatkiše, da budu
                    više fiziči aktivni, da povrate samopouzdanje i smisao svog
                    života, da budu zdraviji, da prestanu da jedu tokom noći, da
                    se pripreme za neki važan događaj u njihovom životu kao što
                    je venčanje, matura i sl.
                  </p>
                  <p className="intro text">
                    Nakon prijave dobićete potvrdu od nas o prijemu iste u roku
                    od 48h!
                  </p>
                  <p className="intro text">
                    Max challenge traje 4 nedelje. Potrebno je da se pridržavate
                    pravila ishrane kao što su da ne jedete posle 19h, pre 10h,
                    da ne jedete slatkiše, da imate tri obroka... Da pešačite
                    minimum 20km svake nedelje. Da se fokusirate na dobre stleti
                    iz svog života. Da se merite svake nedelje. Bićete deo viber
                    grupe gde ćete svakodnevno biti praćeni i usmeravani tokom
                    trajanja challenge-a od strane stručnog tima Max
                    challenge-a...
                  </p>
                  <p className="intro text">
                    A ono što je takođe sjajno je da možeš da osvojiš prelepe
                    nagrade, jer svaki trud i zalaganje itekako nagrađujemo!
                  </p>
                  <p className="intro text">
                    Radujemo se što ćete biti sa nama!
                  </p>
                  <p className="intro text">
                    Molim Vas da iskreno odgovorite na pitanja u nastavku.
                    Napominjemo da će odgovori biti poznati samo nama, da se
                    Vaše ime nigde neće javno objavljivati. Podaci nam trebaju
                    radi upoznavanja grupe i pripreme.
                  </p>
                  <p className="intro text">Sve što hoćemo to i možemo!!!</p>
                  <p className="intro text">
                    Napominjemo da se učešće u Max challenge-u naplaćuje
                    4.000dinara.
                  </p>
                  <br />
                  <br /> <br />
                  <br /> <br />
                  <br />
                  <div className="komentar-levo">
                    <p>Marija Jovanović</p>
                    <p>
                      <i>
                        Napokon sam pronašla motivaciju i uspela da izgubim 7kg!
                        Hvala puno, ovo je bilo predivno iskustvo. Moje
                        preporuke!
                      </i>
                    </p>
                  </div>
                  <br />
                  <br />
                  <br />
                  <div className="komentar-desno">
                    <p>Miloš Manev</p>
                    <p>
                      <i>
                        Pravo mesto da izgubite višak kilograma na zabavan
                        način! Ekipa je divna, vidimo se na sledećem izazovu.
                      </i>
                    </p>
                  </div>
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <div className="komentar-levo">
                    <p>Andrijana Krstić</p>
                    <p>
                      <i>
                        Na izazov sam se prijavila iz radoznalosti, malo je reći
                        da sam oduševljena! Dopalo mi se što na forumu mogu da
                        pokrenem temu koja me interesuje i razmenim iskustva sa
                        ostalim članovima. Sve pohvale za trenera! Nezaboravno
                        iskustvo.
                      </i>
                    </p>
                  </div>
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <div className="komentar-desno">
                    <p>Anica Kostadinović</p>
                    <p>
                      <i>
                        Baza recepata je moj omiljeni kutak na ovom sajtu. Već
                        3. put učestvujem, nadam se pobedi u sledećem terminu!
                      </i>
                    </p>
                  </div>
                  <br />
                  <br />
                  <br />
                </div>
                <div className="col-md-3">
                  <p className="drustvene-mreze"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect()(Home);
