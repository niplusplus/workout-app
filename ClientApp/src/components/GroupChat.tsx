import * as React from "react";
import { connect } from "react-redux";
import OneGroupChat from "./smallComponents/OneGroupChat";

const GroupChat = () => (
  <div>
    <div className="forum">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <br />
            <br />
            <h1 className="text">Grupni chat</h1>
            <br />
            <br />
            <br />

            <OneGroupChat />
            <br />
          </div>
          <div className="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
);

export default connect()(GroupChat);
