import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Axios from "axios";

class Korisniciadmin extends React.PureComponent {
  state = {
    values: [],
  };

  componentDidMount() {
    Axios.get("http://localhost:5000/api/Users/list").then((response) => {
      this.setState({
        values: response.data,
      });
    });
  }

  deleteUser(id: any): void {
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:5000/api/Users/" + id;
    alert(url);
    xhr.open("DELETE", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader(
      "Authorization",
      "Bearer " + localStorage.getItem("tokenUser")
    );

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        window.location.replace("/adminpanel");
      } else {
        //alert("ne radi");
      }
    };
    xhr.send(null);

    this.setState({});
  }

  public render() {
    return (
      <React.Fragment>
        <div>
          <div className="container-fluid prijava-profil">
            <div className="row">
              <div className="col-md-4"></div>

              <div className="col-md-4">
                <br />
                <h1 className="text">Svi korisnici</h1>
                <br />
                {this.state.values.map((value: any) => (
                  <div key={value.id}>
                    <br />
                    <div className="tema">
                      <h1
                        className="xright"
                        onClick={() => this.deleteUser(value.id)}
                      >
                        x
                      </h1>
                      <p className="text-forum">
                        <span className="pinkspan">Ime i prezime:&nbsp;</span>
                        {value.fullName}
                      </p>
                      <p className="text-forum">
                        <span className="pinkspan">Nadimak:&nbsp;</span>
                        {value.nickName}{" "}
                      </p>
                      <p className="text-forum">
                        <span className="pinkspan">Tezima u kg:&nbsp;</span>
                        {value.weight}
                      </p>
                      <p className="text-forum">
                        <span className="pinkspan">
                          Obim struka u cm:&nbsp;
                        </span>
                        {value.waistSize}{" "}
                      </p>
                      <br />
                      <Link
                        className="rightUser"
                        to={`/userdetails/${value.id}`}
                        style={{ textDecoration: "none" }}
                        key={value.id}
                      >
                        saznaj vise &gt;
                      </Link>
                    </div>
                  </div>
                ))}
              </div>
              <div className="col-md-4"></div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect()(Korisniciadmin);
