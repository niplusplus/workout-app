import * as React from "react";
import { connect } from "react-redux";

import { Link, RouteComponentProps } from "react-router-dom";
import Axios from "axios";

interface IProfile {
  id: any;
  numberOfSteps: any;
  weight: any;
  WaistSize: any;
  goodDeed1: any;
  goodDeed2: any;
  goodDeed3: any;
  goodDeed4: any;
  goodDeed5: any;
  weightAtStart: string;
  weistSizeAtStart: string;
  wantedWeight: any;
  avgStepsPerDay: any;
  allStepsOnProgram: any;
  lostCm: any;
  lostWeight: any;
  ProfileImgPath: any;
  file: any;
  image: any;
  selectedFile: any;
  prifile: any;
  fileKoraka: any;
  fileProgresa: any;
}

interface ProfileProps extends RouteComponentProps<IProfile> {}

class Profile extends React.PureComponent<ProfileProps> {
  state = {
    numberOfSteps: 0,
    weight: 0,
    WaistSize: 0,
    goodDeed1: "",
    goodDeed2: "",
    goodDeed3: "",
    goodDeed4: "",
    goodDeed5: "",
    weightAtStart: "",
    weistSizeAtStart: "",
    wantedWeight: "",
    avgStepsPerDay: "",
    allStepsOnProgram: "",
    lostCm: "",
    lostWeight: "",
    ProfileImgPath: localStorage.getItem("profileImgPathUser"),
    file: "",
    selectedFile: null,
    prifile: "",
    fileKoraka: "",
    fileProgresa: "",
  };

  componentDidMount() {
    Axios.get("http://localhost:5000/api/Users/OnProgram", {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("tokenUser"),
      },
    }).then((response) => {
      this.setState({
        values: response.data,
        weightAtStart: response.data["weightAtStart"],
        weistSizeAtStart: response.data["weistSizeAtStart"],
        wantedWeight: response.data["wantedWeight"],
        avgStepsPerDay: response.data["avgStepsPerDay"],
        allStepsOnProgram: response.data["allStepsOnProgram"],
        lostCm: response.data["lostCm"],
        lostWeight: response.data["lostWeight"],
        ProfileImgPath: response.data["ProfileImgPath"],
      });
    });
  }

  daysSubmit(e: any): void {
    e.preventDefault();

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:5000/api/measures/";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader(
      "Authorization",
      "Bearer " + localStorage.getItem("tokenUser")
    );
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        window.location.replace("/tasks");
      }
    };

    this.setState({});

    var data = JSON.stringify({
      numberOfSteps: this.state.numberOfSteps,
      weight: this.state.weight,
      WaistSize: this.state.WaistSize,
      goodDeed1: this.state.goodDeed1,
      goodDeed2: this.state.goodDeed2,
      goodDeed3: this.state.goodDeed3,
      goodDeed4: this.state.goodDeed4,
      goodDeed5: this.state.goodDeed5,
    });

    xhr.send(data);
  }

  handleFile(e: any): void {
    console.log(e.target.files, "$$$$");
    console.log(e.target.files[0], "$$$$");

    let file = e.target.files[0];
    this.setState({
      file: file,
    });
  }

  handleFileKoraka(e: any): void {
    console.log(e.target.files, "$$$$");
    console.log(e.target.files[0], "$$$$");

    let fileKoraka = e.target.files[0];
    this.setState({
      fileKoraka: fileKoraka,
    });
  }

  handleFileProgresa(e: any): void {
    console.log(e.target.files, "$$$$");
    console.log(e.target.files[0], "$$$$");

    let fileProgresa = e.target.files[0];
    this.setState({
      fileProgresa: fileProgresa,
    });
  }

  fileUploadHandler = () => {
    let file = this.state.file;
    let formdata = new FormData();

    formdata.append("Image", file);

    Axios({
      url: "http://localhost:5000/api/Users/profileImage",
      method: "PUT",
      headers: {
        authorization: "Bearer " + localStorage.getItem("tokenUser"),
      },
      data: formdata,
    }).then((res) => {
      Axios({
        url: "http://localhost:5000/api/Users",
        method: "GET",
        headers: {
          authorization: "Bearer " + localStorage.getItem("tokenUser"),
        },
      }).then((response) => {
        localStorage.setItem(
          "profileImgPathUser",
          response.data.profileImgPath
        );
        window.location.replace("/profile");
      });
    });
  };

  fileUploadHandlerKoraka = () => {
    let fileKoraka = this.state.fileKoraka;
    let formdataKoraka = new FormData();

    formdataKoraka.append("Image", fileKoraka);

    Axios({
      url: "http://localhost:5000/api/measures/proofImage",
      method: "PUT",
      headers: {
        authorization: "Bearer " + localStorage.getItem("tokenUser"),
      },
      data: formdataKoraka,
    }).then((res) => {
      window.location.replace("/tasks");
    });
  };

  fileUploadHandlerProgresa = () => {
    let fileProgresa = this.state.fileProgresa;
    let formdataProgresa = new FormData();

    formdataProgresa.append("Image", fileProgresa);

    Axios({
      url: "http://localhost:5000/api/measures/progressImage",
      method: "PUT",
      headers: {
        authorization: "Bearer " + localStorage.getItem("tokenUser"),
      },
      data: formdataProgresa,
    }).then((res) => {
      window.location.replace("/tasks");
    });
  };

  public render() {
    return (
      <React.Fragment>
        <div>
          <div className="prijava-profil-posle-logovanja">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-1"></div>

                <div className="col-md-2">
                  <br />
                  <p className="text-left">
                    <span className="whitespan">
                      {localStorage.getItem("fullNameUser")}
                    </span>
                  </p>

                  <img
                    className=""
                    src={
                      "img/UserProfileImages/" +
                      localStorage.getItem("profileImgPathUser")
                    }
                    width="150"
                    height="150"
                  />

                  <br />
                  <br />
                  <input
                    type="file"
                    name="file"
                    className="profileBtn"
                    onChange={(e) => this.handleFile(e)}
                  />
                  <br />
                  <br />
                  <button
                    onClick={this.fileUploadHandler}
                    className="profileBtn"
                  >
                    Objavi sliku
                  </button>

                  <p className="text-left"></p>
                  <br />
                  <p className="text-left">
                    Kilaža pre početka u kilogramima:
                    <span className="whitespan">
                      {this.state.weightAtStart}
                    </span>
                  </p>
                  <p className="text-left">
                    Obim struka pre početka u sentimetrima:
                    <span className="whitespan">
                      {this.state.weistSizeAtStart}
                    </span>
                  </p>
                  <p className="text-left">
                    Željena kilaža u kilogramima:
                    <span className="whitespan">{this.state.wantedWeight}</span>
                  </p>
                  <p className="text-left">
                    Prosečan broj koraka dnevno:
                    <span className="whitespan">
                      {this.state.avgStepsPerDay}
                    </span>
                  </p>
                  <p className="text-left">
                    Ukupan broj koraka:
                    <span className="whitespan">
                      {this.state.allStepsOnProgram}
                    </span>
                  </p>
                  <p className="text-left">
                    Izgubljeni centimetri:
                    <span className="whitespan">{this.state.lostCm}</span>
                  </p>
                  <p className="text-left">
                    Izgubljeni kilogrami:
                    <span className="whitespan">{this.state.lostWeight}</span>
                  </p>
                </div>
                <div className="col-md-4">
                  <br />
                  <br />
                  <p className="white">Sliku koraka priložite ovde </p>
                  <input
                    type="file"
                    name="file"
                    className="profileBtn"
                    onChange={(e) => this.handleFileKoraka(e)}
                  />
                  <br />
                  <br />
                  <button
                    onClick={this.fileUploadHandlerKoraka}
                    className="profileBtn"
                  >
                    Pošalji sliku koraka
                  </button>
                  <form onSubmit={(e) => this.daysSubmit(e)}>
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="Broj koraka"
                      onChange={(e) =>
                        this.setState({ numberOfSteps: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="Broj kilograma"
                      onChange={(e) =>
                        this.setState({ weight: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="Obim struka"
                      onChange={(e) =>
                        this.setState({ WaistSize: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="1. dobra stvar"
                      onChange={(e) =>
                        this.setState({ goodDeed1: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="2. dobra stvar"
                      onChange={(e) =>
                        this.setState({ goodDeed2: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="3. dobra stvar"
                      onChange={(e) =>
                        this.setState({ goodDeed3: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="4. dobra stvar"
                      onChange={(e) =>
                        this.setState({ goodDeed4: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <input
                      type="text"
                      placeholder="5. dobra stvar"
                      onChange={(e) =>
                        this.setState({ goodDeed5: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <br />
                    <p className="intro text" id="usepsno"></p>
                    <br />

                    <section className="intro">
                      <button
                        id="js-trigger-overlay"
                        className="intro"
                        type="submit"
                      >
                        Postavi
                      </button>
                    </section>
                  </form>
                  <br />
                  <br />
                </div>
                <div className="col-md-4">
                  <br />
                  <br />
                  <p className="white">
                    Sliku nedeljnog progresa priložite ovde
                  </p>
                  <input
                    type="file"
                    name="file"
                    className="profileBtn"
                    onChange={(e) => this.handleFileProgresa(e)}
                  />
                  <br />
                  <br />
                  <button
                    onClick={this.fileUploadHandlerProgresa}
                    className="profileBtn"
                  >
                    Pošalji sliku progresa
                  </button>
                  <br />
                  <br />
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect()(Profile);
