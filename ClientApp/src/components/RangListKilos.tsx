import * as React from "react";
import { connect } from "react-redux";
import {
  Collapse,
  Container,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";
import profilImage from "../img/profil.png";
import OneRanglistKilos from "./smallComponents/OneRanglistKilos";
import OneRanglistSteps from "./smallComponents/OneRanglistSteps";

const RangListKilos = () => (
  <div>
    <div className="ranglista">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <br />
            <br />
            <h1 className="text">Rang lista</h1>
            <br />
            <ul className="navbar-nav flex-grow">
              <NavLink tag={Link} className="nav center" to="/rangList">
                Najviše koraka
              </NavLink>
              <NavLink
                tag={Link}
                className="nav center whiteRangList"
                to="/ranglistkilos"
              >
                Najviše izgubljenih kilograma u procentima
              </NavLink>
              <NavLink tag={Link} className="nav center" to="/ranglistdeeds">
                Najviše dobrih stvari
              </NavLink>
              <NavLink tag={Link} className="nav center" to="/ranglistkm">
                Najviše predjenih kilometara
              </NavLink>
            </ul>
            <OneRanglistKilos />

            <br />
            <br />
          </div>
          <div className="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
);

export default connect()(RangListKilos);
