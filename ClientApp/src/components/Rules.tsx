import * as React from "react";
import { connect } from "react-redux";

const Rules = () => (
  <div>
    <div className="pravila">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-6">
            <br />
            <br />
            <h1 className="intro text">Max challenge</h1>
            <br />
            <br />
            <p className="intro text">
              Hvala Vam na interesovanju za Max challenge. Raduje nas što je
              više od 900 ljudi na ovaj način unapredilo svoj život i izgubilo
              suvišne kilograme, steklo nove pravilne navike u ishrani, postalo
              fizički aktivno, zadovoljno sobom i samouvereno.
            </p>
            <p className="intro text">
              Najčešći razlozi zbog kojih ljudi učestvuju u Max challengu su:
              želja da smršaju, da prestanu da jedu slatkiše, da budu više
              fiziči aktivni, da povrate samopouzdanje i smisao svog života, da
              budu zdraviji, da prestanu da jedu tokom noći, da se pripreme za
              neki važan događaj u njihovom životu kao što je venčanje, matura i
              sl.
            </p>
            <p className="intro text">
              Nakon prijave dobićete potvrdu od nas o prijemu iste u roku od
              48h!
            </p>
            <p className="intro text">
              Max challenge traje 4 nedelje. Potrebno je da se pridržavate
              pravila ishrane kao što su da ne jedete posle 19h, pre 10h, da ne
              jedete slatkiše, da imate tri obroka... Da pešačite minimum 20km
              svake nedelje. Da se fokusirate na dobre stvari iz svog života. Da
              se merite svake nedelje. Bićete deo viber grupe gde ćete
              svakodnevno biti praćeni i usmeravani tokom trajanja challenge-a
              od strane stručnog tima Max challenge-a...
            </p>
            <p className="intro text">
              A ono što je takođe sjajno je da možeš da osvojiš prelepe nagrade,
              jer svaki trud i zalaganje itekako nagrađujemo!
            </p>
            <p className="intro text">Radujemo se što ćete biti sa nama!</p>
            <p className="intro text">
              Molim Vas da iskreno odgovorite na pitanja u nastavku. Napominjemo
              da će odgovori biti poznati samo nama, da se Vaše ime nigde neće
              javno objavljivati. Podaci nam trebaju radi upoznavanja grupe i
              pripreme.
            </p>
            <p className="intro text">Sve što hoćemo to i možemo!!!</p>
            <p className="intro text">
              Napominjemo da se učešće u Max challenge-u naplaćuje 4.000dinara.
            </p>
            <br />
            <br /> <br />
            <br /> <br />
            <br />
          </div>
          <div className="col-md-3"></div>
        </div>
      </div>
    </div>
  </div>
);

export default connect()(Rules);
