import * as React from "react";
import { connect } from "react-redux";

interface IStateLogin {
  email: string;
  password: string;
}

class Login extends React.PureComponent<{}, IStateLogin> {
  constructor(props: {}) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
  }

  loginSubmit(e: any): void {
    e.preventDefault();

    var errorp = document.getElementById("error") as HTMLParagraphElement;

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:5000/api/Users/login";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);

        localStorage.setItem("tokenUser", json.token);
        localStorage.setItem("fullNameUser", json.fullName);
        localStorage.setItem("profileImgPathUser", json.profileImgPath);
        localStorage.setItem("instructorUser", json.isInstructor);

        if (!json.isInstructor) {
          window.location.replace("/profile/" + json.token);
        } else {
          window.location.replace("/adminpanel");
        }
      }

      if (xhr.status === 401) {
        errorp.innerHTML = "Pogresili ste email ili sifru!";
      }
    };

    var data = JSON.stringify({
      email: this.state.email,
      password: this.state.password,
    });

    xhr.send(data);

    this.setState({});
  }

  public render() {
    return (
      <React.Fragment>
        <div>
          <div className="prijava-profil">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-4"></div>
                <div className="col-md-4">
                  <br />
                  <br />
                  <h1 className="text ">Logovanje</h1>
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <form onSubmit={(e) => this.loginSubmit(e)}>
                    <p className="text">Email</p>
                    <input
                      type="email"
                      id=""
                      name=""
                      required
                      onChange={(e) => this.setState({ email: e.target.value })}
                    />
                    <br />
                    <br />
                    <br />
                    <p className="text">Sifra</p>
                    <input
                      type="password"
                      id=""
                      name=""
                      required
                      onChange={(e) =>
                        this.setState({ password: e.target.value })
                      }
                    />
                    <br />
                    <br />
                    <br />
                    <p id="error"> </p>
                    <section className="intro">
                      <button
                        id="js-trigger-overlay"
                        className="intro"
                        type="submit"
                      >
                        Prijavi se
                      </button>
                    </section>
                  </form>
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                  <br />
                </div>
                <div className="col-md-4"></div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default connect()(Login);
