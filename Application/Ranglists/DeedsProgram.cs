using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Ranglists
{
    public class DeedsProgram
    {
        public class Query : IRequest<List<RanglistDTO>> { }
        
        public class Handler : IRequestHandler<Query, List<RanglistDTO>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<List<RanglistDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var measures = await _context.Measures.GroupBy(u => u.User.UserName, s => s.DeedCounter, (key, g) => new RanglistDTO{
                    UserName = key,
                    Value = g.Sum()
                }).OrderByDescending(x => x.Value).ToListAsync(); 
                return measures;
            }
        }
    }
}