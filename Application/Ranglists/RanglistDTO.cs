namespace Application.Ranglists
{
    public class RanglistDTO
    {
        public string UserName { get; set; }
        public double Value { get; set; }
    }
}