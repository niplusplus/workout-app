using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Ranglists
{
    public class WaistSizeProgram
    {
        public class Query : IRequest<List<RanglistDTO>> { }
        
        public class Handler : IRequestHandler<Query,List<RanglistDTO>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<List<RanglistDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var ranglist = await _context.UsersInPrograms.Include(x => x.User).Select(x => new RanglistDTO{
                    UserName = x.User.UserName,
                    Value = x.LostCm * -1,
                }).OrderBy(x => x.Value).ToListAsync();
                return ranglist;
            }
        }
    }
}