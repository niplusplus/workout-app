using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.GroupTasks
{
    public class List
    {
        public class Query : IRequest<List<GroupTask>> { }
        
                public class Handler : IRequestHandler<Query, List<GroupTask>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<GroupTask>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        return await _context.GroupTasks.OrderByDescending(x => x.Timestamp).ToListAsync();
                    }
                }
    }
}