using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using MediatR;
using Persistence;

namespace Application.GroupTasks
{
    public class Details
    {
        public class Query : IRequest<GroupTask> { 
            public Guid Id { get; set; }
        }
        
                public class Handler : IRequestHandler<Query, GroupTask>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<GroupTask> Handle(Query request, CancellationToken cancellationToken)
                    {
                        var task = await _context.GroupTasks.FindAsync(request.Id);
                        if(task == null)
                            throw new RestException(HttpStatusCode.NotFound, new {task = "Not found"});
                        return task;
                    }
                }
    }
}