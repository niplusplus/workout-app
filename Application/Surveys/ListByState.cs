using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Surveys
{
    public class ListByState
    {
        public class Query : IRequest<List<Survey>> {
            public String State { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, List<Survey>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<Survey>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        return await _context.Surveys.Where(x => x.State == request.State).ToListAsync();
                    }
                }
    }
}