using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Surveys
{
    public class Create
    {
        public class Command : IRequest
                {
                    public String Fullname { get; set; }
                    public String City {get; set; }
                    public String Address {get; set; }
                    public String Birth { get; set; }
                    public String Number { get; set; }
                    public String Motive { get; set;}
                    public String Obligations { get; set; }
                    public String Sweets { get; set; }
                    public String Diet { get; set; }
                    public String Difficulty { get; set; }
                    public String Medicines { get; set; }
                    public String Opinion { get; set; }
                    public String Mistakes { get; set; }
                    public String Weight { get; set; }
                    public String Height { get; set; }
                    public String MagicNumber { get; set; }
                    public String Training { get; set; }
                    public String Measurement { get; set; }
                    public String Questions { get; set; }
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var survey = new Survey{
                            Fullname = request.Fullname,
                            City = request.City,
                            Address = request.Address,
                            Birth = request.Birth,
                            Number = request.Number,
                            Motive = request.Motive,
                            Obligations = request.Obligations,
                            Sweets = request.Sweets,
                            Diet = request.Diet,
                            Difficulty = request.Difficulty,
                            Medicines = request.Medicines,
                            Opinion = request.Opinion,
                            Mistakes = request.Mistakes,
                            Weight = request.Weight,
                            Height = request.Height,
                            MagicNumber = request.MagicNumber,
                            Training = request.Training,
                            Measurement = request.Measurement, 
                            Questions = request.Questions,
                            State = "unreviewed",
                        };
                        _context.Surveys.Add(survey);
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}