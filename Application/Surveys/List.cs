using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Surveys
{
    public class List
    {
        public class Query : IRequest<List<Survey>> { }
        
                public class Handler : IRequestHandler<Query, List<Survey>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<Survey>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        return await _context.Surveys.ToListAsync();
                    }
                }
    }
}