using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using MediatR;
using Persistence;

namespace Application.Surveys
{
    public class Details
    {
        public class Query : IRequest<Survey> {
            public Guid Id { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, Survey>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<Survey> Handle(Query request, CancellationToken cancellationToken)
                    {
                        var survey = await _context.Surveys.FindAsync(request.Id);
                        if (survey == null)
                            throw new RestException(HttpStatusCode.NotFound, new { survey = "Not found" });
                        return survey; 
                    }
                }
    }
}