using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using MediatR;
using Persistence;

namespace Application.Surveys
{
    public class Delete
    {
        public class Command : IRequest
                {
                    public Guid Id { get; set; } 
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var survey = await _context.Surveys.FindAsync(request.Id);
                        if(survey == null)
                            throw new RestException(HttpStatusCode.NotFound, new {survey = "Not found"});
                        
                        _context.Remove(survey);
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}