﻿using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ForumThemes
{
    public class List
    {
        public class Query : IRequest<List<ForumTheme>>
        {
            public string category{ get; set; }
        }


        public class Handler : IRequestHandler<Query, List<ForumTheme>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<ForumTheme>> Handle(Query request, CancellationToken cancellationToken)
            {
                var forumThemes = new List<ForumTheme>();
                if (request.category != "all")
                {
                    forumThemes = await _context.ForumThemes.Where(x => x.Category == request.category).OrderByDescending(x => x.CreateDate).ToListAsync();
                }
                else {
                    forumThemes = await _context.ForumThemes.ToListAsync();
                }
                
                return forumThemes;
            }
        }
    }
}
