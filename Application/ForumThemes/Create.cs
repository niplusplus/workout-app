using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MediatR;
using Persistence;

namespace Application.ForumThemes
{
    public class Create
    {
        public class Command : IRequest
        {
            public string Title { get; set; }
            public string Category { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var ownerName = _userAccessor.GetCurrentUsername();
                var forumTheme = new ForumTheme
                {
                    Title = request.Title,
                    OwnerName = ownerName,
                    CreateDate = DateTime.Now,
                    NumberOfMessages = 0,
                    Category = request.Category
                };
                _context.ForumThemes.Add(forumTheme);
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}