using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using MediatR;
using Persistence;

namespace Application.ForumThemes
{
    public class Delete
    {
        public class Command : IRequest
                {
                    public Guid Id;
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var theme = await _context.ForumThemes.FindAsync(request.Id);
                        if(theme == null)
                            throw new RestException(HttpStatusCode.NotFound, new {theme = "Not found"});
                        
                        _context.Remove(theme);
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}