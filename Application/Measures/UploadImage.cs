using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Persistence;

namespace Application.Measures
{
    public class UploadImage
    {
        public class Command : IRequest
        {
            public IFormFile Image { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            private readonly UserManager<User> _userManager;
            public Handler(DataContext context, UserManager<User> userManager, IUserAccessor userAccessor)
            {
                this._userManager = userManager;
                this._userAccessor = userAccessor;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                var measure = _context.Measures.Where(x => x.UserId == user.Id).OrderByDescending(m => m.Created).FirstOrDefault();
                String fileName = user.UserName + "_" + DateTime.Now.ToString("s");
                fileName = fileName.Replace(":", "-");
                String path = Directory.GetParent(Directory.GetCurrentDirectory()) + "\\ClientApp\\public\\img\\MeasureImages\\";
                if (request.Image != null)
                {
                    fileName += Path.GetExtension(request.Image.FileName);
                    path = Path.Combine(path, fileName);
                    using (var fs = new FileStream(path, FileMode.Create))
                    {
                        await request.Image.CopyToAsync(fs);
                    }
                    measure.ImagePath = fileName;
                }
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}