using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Measures
{
    public class UserStepsImage
    {
        public class Query : IRequest<List<MeasurePhotoDTO>> {
            public String Id { get; set; }
         }

        public class Handler : IRequestHandler<Query, List<MeasurePhotoDTO>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }
            public async Task<List<MeasurePhotoDTO>> Handle(Query request, CancellationToken cancellationToken)
            {                
                return _mapper.Map<List<Measure>, List<MeasurePhotoDTO>>(await _context.Measures.Where(x => x.UserId == request.Id && x.ImagePath != null).ToListAsync<Measure>());
            }
        }
    }
}