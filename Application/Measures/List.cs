using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Measures
{
    public class List
    {
        public class Query : IRequest<List<MeasureDTO>> { }

        public class Handler : IRequestHandler<Query, List<MeasureDTO>>
        {
            private readonly DataContext _context;
            private readonly UserManager<User> _userManager;
            private readonly IUserAccessor _userAccessor;
            private readonly IMapper _mapper;
            public Handler(DataContext context, UserManager<User> userManager, IUserAccessor userAccessor, IMapper mapper)
            {
                this._mapper = mapper;
                this._userAccessor = userAccessor;
                this._userManager = userManager;
                this._context = context;
            }

            public UserManager<User> UserManager { get; }
            public IUserAccessor UserAccessor { get; }

            public async Task<List<MeasureDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                return _mapper.Map<List<Measure>, List<MeasureDTO>>(await _context.Measures.Where(m => m.UserId == user.Id).ToListAsync<Measure>());
            }
        }
    }
}