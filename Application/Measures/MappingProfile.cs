using Application.Messages;
using Application.Programs;
using Application.Users;
using AutoMapper;
using Domain;

namespace Application.Measures
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Measure, MeasureDTO>();
            CreateMap<User, AppUser>()
                .ForMember(d => d.NickName, o => o.MapFrom(s => s.UserName));
            CreateMap<Program, ProgramDTO>();
            CreateMap<UserInProgram, UserInProgramDTO>();
            CreateMap<Message, MessageDTO>();
            CreateMap<Measure, MeasurePhotoDTO>()
                .ForMember(d => d.imagePath, o => o.MapFrom(s => s.ImagePath));
            CreateMap<Measure, MeasurePhotoUserProgressDTO>()
                .ForMember(d => d.imagePath, o => o.MapFrom(s => s.UserProgressImagePath));
        }
    }
}