using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Measures
{
    public class ListByUser
    {
        public class Query : IRequest<List<MeasureDTO>>
        {
            public String Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, List<MeasureDTO>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }

            public async Task<List<MeasureDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                return _mapper.Map<List<Measure>, List<MeasureDTO>>(await _context.Measures.Where(m => m.UserId == request.Id).ToListAsync<Measure>());
            }
        }
    }
}