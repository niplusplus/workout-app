using System;
using Application.Users;

namespace Application.Measures
{
    public class MeasureDTO
    {
        public Guid Id {get; set;}
        public int NumberOfSteps { get; set; }
        public int Weight { get; set; }
        public int WaistSize { get; set; }
        public string GoodDeed1 { get; set; }
        public string GoodDeed2 { get; set; }
        public string GoodDeed3 { get; set; }
        public string GoodDeed4 { get; set; }
        public string GoodDeed5 { get; set; }
        public int DeedCounter { get; set; }
        public String ImagePath { get; set; }
        public string UserProgressImagePath { get; set; }
        public AppUser User { get; set; }
        public DateTime Created { get; set; }
    }
}