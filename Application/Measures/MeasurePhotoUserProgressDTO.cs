using System;

namespace Application.Measures
{
    public class MeasurePhotoUserProgressDTO
    {
        public Guid Id { get; set; }
        public String imagePath { get; set; }
    }
}