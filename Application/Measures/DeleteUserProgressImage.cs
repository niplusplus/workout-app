using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Measures
{
    public class DeleteUserProgressImage
    {
         public class Command : IRequest
                {
                    public Guid Id { get; set; }
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var measure = await _context.Measures.Where(x => x.Id == request.Id).FirstOrDefaultAsync();
                        string fileName = measure.UserProgressImagePath;
                        string rootFolder = Directory.GetParent(Directory.GetCurrentDirectory()) + "\\ClientApp\\public\\img\\MeasureImages\\";
                        measure.UserProgressImagePath = null;
                        try    
                        {    
                        // Check if file exists with its full path    
                        if (File.Exists(Path.Combine(rootFolder, fileName)))    
                        {    
                        // If file found, delete it    
                        File.Delete(Path.Combine(rootFolder, fileName));    
                        Console.WriteLine("File deleted.");    
                        }    
                        else Console.WriteLine("File not found");    
                        }    
                        catch (IOException ioExp)    
                        {    
                        Console.WriteLine(ioExp.Message);    
                        }    
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}