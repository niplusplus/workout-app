using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Measures
{
    public class Create
    {
        public class Command : IRequest
        {
            public int NumberOfSteps { get; set; }
            public int Weight { get; set; }
            public int WaistSize { get; set; }
            public string GoodDeed1 { get; set; }
            public string GoodDeed2 { get; set; }
            public string GoodDeed3 { get; set; }
            public string GoodDeed4 { get; set; }
            public string GoodDeed5 { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {

                var measure = new Measure
                {
                    Id = Guid.NewGuid(),
                    NumberOfSteps = request.NumberOfSteps,
                    Weight = request.Weight,
                    WaistSize = request.WaistSize,
                    GoodDeed1 = request.GoodDeed1,
                    GoodDeed2 = request.GoodDeed2,
                    GoodDeed3 = request.GoodDeed3,
                    GoodDeed4 = request.GoodDeed4,
                    GoodDeed5 = request.GoodDeed5,
                    Created = DateTime.Now
                };

                var user = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());
                var deedConuter = 0;
                if(request.GoodDeed1 != null && request.GoodDeed1.Length > 1){
                    deedConuter++;
                }
                if(request.GoodDeed2 != null && request.GoodDeed2.Length > 1){
                    deedConuter++;
                }
                if(request.GoodDeed3 != null && request.GoodDeed3.Length > 1){
                    deedConuter++;
                }
                if(request.GoodDeed4 != null && request.GoodDeed4.Length > 1){
                    deedConuter++;
                }
                if(request.GoodDeed5 != null && request.GoodDeed5.Length > 1){
                    deedConuter++;   
                }
                measure.DeedCounter = deedConuter;
                measure.User = user;
                measure.UserId = user.Id;
                user.Weight = request.Weight;
                user.WaistSize = request.WaistSize;
                var userInProgram = await _context.UsersInPrograms.Include(x => x.Program).SingleOrDefaultAsync((x => x.UserId == user.Id && (x.Program.StartingDate).CompareTo(DateTime.Now) < 0 && (x.Program.EndingDate).CompareTo(DateTime.Now) > 0));
                if(userInProgram != null){
                    userInProgram.AllStepsOnProgram = userInProgram.AllStepsOnProgram + request.NumberOfSteps;
                    userInProgram.AvgStepsPerDay = (int)(userInProgram.AllStepsOnProgram / (DateTime.Now - (userInProgram.Program.StartingDate)).TotalDays);
                    userInProgram.LostCm = 100*(userInProgram.WeistSizeAtStart - request.WaistSize) / userInProgram.WeistSizeAtStart;
                    userInProgram.LostWeight = 100*(userInProgram.WeightAtStart - request.Weight) / userInProgram.WeightAtStart;
                }
                _context.Measures.Add(measure);
                var success = await _context.SaveChangesAsync() > 0;

                if (success){
                    return Unit.Value;
                }

                throw new Exception("Problem saving changes");
            }
        }
    }
}