using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Persistence;

namespace Application.Measures
{
    public class Last
    {
        public class Query : IRequest<MeasureDTO>
        {
        }

        public class Handler : IRequestHandler<Query, MeasureDTO>
        {
            private readonly DataContext _context;
            private readonly UserManager<User> _userManager;
            private readonly IUserAccessor _userAccessor;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IUserAccessor userAccessor, UserManager<User> userManager, IMapper mapper)
            {
                this._mapper = mapper;
                this._userAccessor = userAccessor;
                this._userManager = userManager;
                this._context = context;
            }

            public async Task<MeasureDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                var measure = _context.Measures.Where(x => x.UserId == user.Id).OrderByDescending(m => m.Created).FirstOrDefault();
                if (measure == null)
                    throw new RestException(HttpStatusCode.NotFound, new { measure = "Not found" });
                return _mapper.Map<Measure, MeasureDTO>(measure);
            }
        }
    }
}