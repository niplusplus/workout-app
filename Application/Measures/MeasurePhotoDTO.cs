using System;

namespace Application.Measures
{
    public class MeasurePhotoDTO
    {
        public Guid Id { get; set; }
        public String imagePath { get; set; }
    }
}