using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Recipes
{
    public class UploadImage
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public IFormFile Image { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var recipe = await _context.Recipes.FirstOrDefaultAsync(x => x.Id == request.Id);
                if(recipe == null){
                    throw new RestException(HttpStatusCode.NotFound, new { recipe = "Not found" });
                }
                String fileName = recipe.Title + "_" + DateTime.Now.ToString("s") + "_";
                fileName = fileName.Replace(":", "-");
                String path = Directory.GetParent(Directory.GetCurrentDirectory()) + "\\ClientApp\\public\\img\\RecipesImages\\";
                if(request.Image != null){
                    fileName += request.Image.FileName;
                    path = Path.Combine(path, fileName);
                    using (var fs = new FileStream(path, FileMode.Create)){
                        await request.Image.CopyToAsync(fs);
                    }
                    recipe.ImagePath = fileName;
                }
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}