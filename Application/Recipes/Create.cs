using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Persistence;

namespace Application.Recipes
{
    public class Create
    {
        public class Command : IRequest
        {
            public String Title { get; set; }
            public String ShortDescription { get; set; }
            public String Ingredients { get; set; }
            public String Description { get; set; }
            public String Category { get; set; }
            public String TimeToPrepare { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly UserManager<User> _userManager;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, UserManager<User> userManager, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._userManager = userManager;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                var state = "pending";
                if(user.isInstructor){
                    state = "approved";
                }
                var recipe = new Recipe
                {
                    Title = request.Title,
                    ShortDescription = request.ShortDescription,
                    Ingredients = request.Ingredients,
                    Description = request.Description,
                    Category = request.Category,
                    TimeToPrepare = request.TimeToPrepare,
                    State = state
                };

                this._context.Recipes.Add(recipe);
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}