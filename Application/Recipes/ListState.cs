using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Recipes
{
    public class ListState
    {
        public class Query : IRequest<List<Recipe>> {
            public String State { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, List<Recipe>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<Recipe>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        var recipes = await _context.Recipes.Where(x => x.State == request.State).ToListAsync();
                        return recipes;
                    }
                }
    }
}