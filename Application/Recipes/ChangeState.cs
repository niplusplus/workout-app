using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using MediatR;
using Persistence;

namespace Application.Recipes
{
    public class ChangeState
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public String State { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var recipe = await _context.Recipes.FindAsync(request.Id);
                if(recipe == null ){
                    throw new RestException(HttpStatusCode.NotFound, new {recipe = "Not found"});
                }
                recipe.State = request.State;
                var success = await _context.SaveChangesAsync() > 0;

                if(success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}