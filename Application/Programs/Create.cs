using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Programs
{
    public class Create
    {
        public class Command : IRequest
                {
                    public String StartingDate { get; set; }
                    public String EndingDate { get; set; }
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var program = new Program{
                            StartingDate = DateTime.Parse(request.StartingDate),
                            EndingDate = DateTime.Parse(request.EndingDate)
                        };
                        _context.Programs.Add(program);
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}