using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Programs
{
    public class List
    {
        public class Query : IRequest<List<Program>> { }
        
                public class Handler : IRequestHandler<Query, List<Program>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<Program>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        return await _context.Programs.OrderBy(x => x.StartingDate).ToListAsync();
                    }
                }
    }
}