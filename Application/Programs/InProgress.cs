using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Programs
{
    public class InProgress
    {
        public class Query : IRequest<ProgramDTO> { }

        public class Handler : IRequestHandler<Query, ProgramDTO>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }

            public async Task<ProgramDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                return _mapper.Map<Program, ProgramDTO>(await _context.Programs.Where(x => x.StartingDate.CompareTo(DateTime.Now) < 0 && x.EndingDate.CompareTo(DateTime.Now) > 0).FirstOrDefaultAsync());
            }
        }
    }
}