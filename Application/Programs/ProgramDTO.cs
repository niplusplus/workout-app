using System;
using System.Collections.Generic;

namespace Application.Programs
{
    public class ProgramDTO
    {
        public Guid Id { get; set; }
        public String StartingDate { get; set; }
        public String EndingDate { get; set; }
        public ICollection<UserInProgramDTO> UsersInProgram { get; set; }
    }
}