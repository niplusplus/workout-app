using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Programs
{
    public class Details
    {
        public class Query : IRequest<ProgramDTO>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, ProgramDTO>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;
            }

            public async Task<ProgramDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                return _mapper.Map<Program,ProgramDTO>(await _context.Programs.Include(x => x.UsersInProgram).ThenInclude(x => x.User).Where(x => x.Id == request.Id).FirstOrDefaultAsync());
            }
        }
    }
}