using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Programs
{
    public class AddUserToProgram
    {
        public class Command : IRequest
                {
                    public Guid ProgramId { get; set; }
                    public String UserId { get; set; }
                    public double WantedWeight { get; set; }
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var user = await _context.Users.FindAsync(request.UserId);
                        var program = await _context.Programs.FindAsync(request.ProgramId);
                        var userInProgram = new UserInProgram{
                            User = user,
                            Program = program,
                            WantedWeight = request.WantedWeight,
                            LostCm = 0,
                            AllStepsOnProgram = 0,
                            AvgStepsPerDay = 0,
                            WeightAtStart = user.Weight,
                            WeistSizeAtStart = user.WaistSize,
                        };
                        _context.UsersInPrograms.Add(userInProgram);
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}