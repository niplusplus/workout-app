using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using MediatR;
using Persistence;

namespace Application.ForumMessages
{
    public class Delete
    {
        public class Command : IRequest
        {
            public Guid Id;
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var msg = await _context.ForumMessages.FindAsync(request.Id);
                if(msg == null)
                    throw new RestException(HttpStatusCode.NotFound, new {msg = "Not found"});
                
                var themeId = msg.ForumThemeId;
                var theme = _context.ForumThemes.SingleOrDefault(x => x.Id == themeId);
                theme.NumberOfMessages--;
                _context.Remove(msg);
                var success = await _context.SaveChangesAsync() > 0;

                if(success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}