﻿using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ForumMessages
{
    public class List
    {
        public class Query : IRequest<List<ForumMessage>>
        {
            public Guid ForumThemeId { get; set; }
        }


        public class Handler : IRequestHandler<Query, List<ForumMessage>>
        {
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<ForumMessage>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.ForumMessages.Where(x => x.ForumThemeId == request.ForumThemeId).OrderByDescending(x => x.CreateDate).ToListAsync();
            }
        }
    }
}
