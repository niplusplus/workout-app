﻿using Application.Interfaces;
using Domain;
using MediatR;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.ForumMessages
{
    public class Create
    {
        public class Command : IRequest
        {
            public string Text { get; set; }
            public Guid ForumThemeId { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;

            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var authorName = _userAccessor.GetCurrentUsername();
                var forumMessage = new ForumMessage
                {
                    Text = request.Text,
                    AuthorName = authorName,
                    CreateDate = DateTime.Now,
                    ForumThemeId = request.ForumThemeId
                };
                var forumTheme = _context.ForumThemes.FirstOrDefault(x => x.Id == request.ForumThemeId);
                forumTheme.NumberOfMessages++;
                forumMessage.ForumTheme = forumTheme;
                _context.ForumMessages.Add(forumMessage);
                var success = await _context.SaveChangesAsync() > 0;
                if (success) return Unit.Value;
                throw new Exception("Problem adding Forum message");
            }
        }
    }
}
