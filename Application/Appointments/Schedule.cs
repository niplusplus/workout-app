using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MailKit.Net.Smtp;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using Persistence;

namespace Application.Appointments
{
    public class Schedule
    {
        public class Command : IRequest
        {
            public String Time { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());
                var userAdmin = await _context.Users.FirstOrDefaultAsync(x => x.isInstructor == true);
                var appointment = new Appointment
                {
                    Time = request.Time,
                    State = "pending",
                    UserId = user.Id,
                    UserName = user.UserName,
                    Email = user.Email,
                    FullName = user.FullName,
                    
                };
                _context.Appointments.Add(appointment);
                var success = await _context.SaveChangesAsync() > 0;

                if (success){
                    /*var message = new MimeMessage();
                    message.From.Add(new MailboxAddress(userAdmin.FullName, userAdmin.Email));
                    message.To.Add(new MailboxAddress(user.UserName, user.Email));
                    message.Subject = "Appointment";
                    message.Body = new TextPart("plain"){
                        Text = "Your appointment for " + appointment.Time + ". Has been successfully requested./n We will contact you soon with approval information./nBest Regards./n" + userAdmin.FullName
                    };
                    using (var client = new SmtpClient()){
                        client.Connect("smtp.gmail.com", 587, true);
                        client.Send(message);
                        client.Disconnect(true);
                    }*/
                     return Unit.Value;
                }

                throw new Exception("Problem saving changes");
            }
        }
    }
}