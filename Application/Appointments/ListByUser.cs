using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Appointments
{
    public class ListByUser
    {
        public class Query : IRequest<List<Appointment>> { }

        public class Handler : IRequestHandler<Query, List<Appointment>>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._context = context;
            }

            public async Task<List<Appointment>> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = _context.Users.FirstOrDefault(x => x.UserName == _userAccessor.GetCurrentUsername());
                return await _context.Appointments.Where(x => x.UserId == user.Id).ToListAsync();
            }
        }
    }
}