using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Appointments
{
    public class ListByState
    {
        public class Query : IRequest<List<Appointment>> {
            public String State { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, List<Appointment>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<Appointment>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        return await _context.Appointments.Where(x => x.State == request.State).ToListAsync();
                    }
                }
    }
}