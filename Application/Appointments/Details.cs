using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using MediatR;
using Persistence;

namespace Application.Appointments
{
    public class Details
    {
        public class Query : IRequest<Appointment> {
            public Guid Id { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, Appointment>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<Appointment> Handle(Query request, CancellationToken cancellationToken)
                    {
                        var appointment = await _context.Appointments.FindAsync(request.Id);
                        if(appointment == null)
                            throw new RestException(HttpStatusCode.NotFound, new {appointment = "Not found"});
                        return appointment;
                    }
                }
    }
}