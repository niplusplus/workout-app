using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MailKit.Net.Smtp;
using Persistence;

namespace Application.Appointments
{
    public class Approve
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var appointment = await _context.Appointments.FindAsync(request.Id);
                appointment.State = "approved";
                var userAdmin = await _context.Users.SingleOrDefaultAsync(x => x.UserName == _userAccessor.GetCurrentUsername());
                var user = await _context.Users.FindAsync(appointment.UserId);
                /*var message = new MimeMessage();
                message.From.Add(new MailboxAddress(userAdmin.FullName, userAdmin.Email));
                message.To.Add(new MailboxAddress(user.UserName, user.Email));
                message.Subject = "Appointment";
                message.Body = new TextPart("plain"){
                    Text = "Your appointment for " + appointment.Time + ". Has been successfully scheduled./n We will contant you soon with meeting information./nBest Regards./n" + userAdmin.FullName
                };
                using (var client = new SmtpClient()){
                    client.Connect("smtp.gmail.com", 587, false);
                    client.Send(message);
                    client.Disconnect(true);
                }*/
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}