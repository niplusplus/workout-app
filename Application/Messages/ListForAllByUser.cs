using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Users;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Messages
{
    public class ListForAllByUser
    {
        public class Query : IRequest<List<MessageDTO>> { }

        public class Handler : IRequestHandler<Query, List<MessageDTO>>
        {
            private readonly DataContext _context;
            private readonly IUserAccessor _userAccessor;
            private readonly UserManager<User> _userManager;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IUserAccessor userAccessor, UserManager<User> userManager, IMapper mapper)
            {
                this._mapper = mapper;
                this._userManager = userManager;
                this._userAccessor = userAccessor;
                this._context = context;
            }

            public async Task<List<MessageDTO>> Handle(Query request, CancellationToken cancellationToken)
            {
                var sender = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                var ret =  (await _context.Messages.Where(x => x.Type == "group").OrderBy(x => x.TimeStamp).OrderByDescending(x => x.TimeStamp).ToListAsync());
                foreach(Message m in ret){
                    if(m.Sender == null){
                        var user = await _context.Users.Where(x => x.Id == m.SenderId).FirstOrDefaultAsync();
                        m.Sender = user;
                    }                    
                }                
                return _mapper.Map<List<Message>, List<MessageDTO>>(ret);
            }
        }
    }
}