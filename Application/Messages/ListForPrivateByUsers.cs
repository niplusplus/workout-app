using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Messages
{
    public class ListForPrivateByUsers
    {
        public class Query : IRequest<List<MessageDTO>> {
            public String NickName { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, List<MessageDTO>>
                {
                    private readonly DataContext _context;
                    private readonly IUserAccessor _userAccessor;
                    private readonly UserManager<User> _userManager;
                    private readonly IMapper _mapper;
                    public Handler(DataContext context, IUserAccessor userAccessor, UserManager<User> userManager, IMapper mapper)
                    {
                        this._mapper = mapper;
                        this._userManager = userManager;
                        this._userAccessor = userAccessor;
                        this._context = context;
                    }
        
                    public async Task<List<MessageDTO>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        var sender = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                        var reciver = await _userManager.FindByNameAsync(request.NickName);
                        return _mapper.Map<List<Message>, List<MessageDTO>>(await _context.Messages.Where(x => (x.ReciverId == reciver.Id && x.SenderId == sender.Id) || (x.ReciverId == sender.Id && x.SenderId == reciver.Id)).OrderByDescending(x => x.TimeStamp).ToListAsync());
                    }
                }
    }
}