using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Persistence;

namespace Application.Messages
{
    public class Create
    {
        public class Command : IRequest<Unit>
        {
            public String Text { get; set; }
            public String ReciverNickName { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly UserManager<User> _userManager;
            private readonly IUserAccessor _userAccessor;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IUserAccessor userAccessor, UserManager<User> userManager, IMapper mapper)
            {
                this._mapper = mapper;
                this._userAccessor = userAccessor;
                this._userManager = userManager;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var sender = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                var reciverId = "all";
                var reciver = new User();
                
                var message = new Message();
                if (request.ReciverNickName != "all")
                {
                    reciver = await _userManager.FindByNameAsync(request.ReciverNickName);
                    reciverId = reciver.Id;                    
                    message  = new Message
                    {
                        Sender = sender,
                        Reciver = null,
                        ReciverId = reciverId,
                        Text = request.Text,
                        Type = "private",
                        TimeStamp = DateTime.Now
                    };
                }else{
                    message  = new Message
                    {
                        Sender = sender,
                        Text = request.Text,
                        Type = "group",
                        TimeStamp = DateTime.Now
                    };
                }
                _context.Messages.Add(message);
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}