using System;
using Application.Users;

namespace Application.Messages
{
    public class MessageDTO
    {
        public Guid Id { get; set; }
        public String Text { get; set; }
        public AppUser Sender { get; set; }
        public String SenderId { get; set; }
        public AppUser Reciver { get; set; }
        public String ReciverId { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}