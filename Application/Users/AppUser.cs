using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Users
{
    public class AppUser
    {
        public String Id { get; set; }
        public String FullName { get; set; }
        public String NickName { get; set; }
        public String Email { get; set; }
        public String ProfileImgPath { get; set; }
        public double Weight { get; set; }
        public double WaistSize { get; set; }
        public bool isInstructor { get; set; }
        public string Token {get; set;}
    }
}