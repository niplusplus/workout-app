using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Programs;
using AutoMapper;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Users
{
    public class OnProgram
    {
        public class Query : IRequest<UserInProgramDTO> { }

        public class Handler : IRequestHandler<Query, UserInProgramDTO>
        {
            private readonly DataContext _context;
            private readonly UserManager<User> _userManager;
            private readonly IUserAccessor _userAccessor;
            private readonly IMapper _mapper;
            public Handler(DataContext context, UserManager<User> userManager, IUserAccessor userAccessor, IMapper mapper)
            {
                this._mapper = mapper;
                this._userAccessor = userAccessor;
                this._userManager = userManager;
                this._context = context;
            }

            public async Task<UserInProgramDTO> Handle(Query request, CancellationToken cancellationToken)
            {
                var program = await _context.Programs.Where(x => x.StartingDate.CompareTo(DateTime.Now) < 0 && x.EndingDate.CompareTo(DateTime.Now) > 0).FirstOrDefaultAsync();
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                return _mapper.Map<UserInProgram, UserInProgramDTO>(await _context.UsersInPrograms.Where(x => x.UserId == user.Id && x.ProgramId == program.Id).FirstOrDefaultAsync());

            }
        }
    }
}