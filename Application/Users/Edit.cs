
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using MediatR;
using Persistence;

namespace Application.Users
{
    public class Edit
    {
        public class Command : IRequest
                {
                    public String Id { get; set; }
                    public String FullName { get; set; }
                    public String NickName { get; set; }
                    public String Email { get; set; }
                    public bool? isInstructor { get; set; }
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var user = await _context.Users.FindAsync(request.Id);
                        if(user == null)
                            throw new RestException(HttpStatusCode.NotFound, new {user = "Not found"});
                        user.FullName = request.FullName ?? user.FullName;
                        user.UserName = request.NickName ?? user.UserName;
                        user.Email = request.Email ?? user.Email;
                        user.NormalizedEmail = request.Email.ToUpper() ?? user.NormalizedEmail;
                        user.NormalizedUserName = request.NickName.ToUpper() ?? user.UserName.ToUpper();
                        user.isInstructor = request.isInstructor ?? user.isInstructor;

                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}