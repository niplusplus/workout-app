using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Users
{
    public class Register
    {
        public class Command : IRequest<AppUser>
                {
                    public String FullName { get; set; }
                    public String NickName { get; set; }
                    public string Email { get; set; }
                    public string Password { get; set;  }
                    public double Weight { get; set; }
                    public double WaistSize { get; set; }
                    public String ProfileImgPath { get; set; }
                }
        
                public class Handler : IRequestHandler<Command, AppUser>
                {
                    private readonly DataContext _context;
                    private readonly UserManager<User> userManager;
                    private readonly IJwtGenerator jwtGenerator;

                    public Handler(DataContext context, UserManager<User> userManager, IJwtGenerator jwtGenerator)
                    {
                        this._context = context;
                        this.userManager = userManager;
                        this.jwtGenerator = jwtGenerator;
                    }
        
                    public async Task<AppUser> Handle(Command request, CancellationToken cancellationToken)
                    {
                        
                        if(await _context.Users.Where(x => x.Email == request.Email).AnyAsync()){
                            throw new RestException(HttpStatusCode.BadRequest, new {Email = "Email already exist"});
                        }

                        if(await _context.Users.Where(x => x.UserName == request.NickName).AnyAsync()){
                            throw new RestException(HttpStatusCode.BadRequest, new {NickName = "Nickname already exist"});
                        }

                        var user = new User{
                            FullName = request.FullName,
                            Email = request.Email,
                            UserName = request.NickName,
                            Weight = request.Weight,
                            WaistSize = request.WaistSize,
                            ProfileImgPath = "user.png",
                            isInstructor = false
                        };

                        
                        var result = await userManager.CreateAsync(user, request.Password);
        
                        if(result.Succeeded){ 
                            return new AppUser{
                                Id = user.Id,
                                FullName = user.FullName,
                                NickName = user.UserName,
                                isInstructor = user.isInstructor,
                                Weight = user.Weight,
                                WaistSize = user.WaistSize,
                                ProfileImgPath = user.ProfileImgPath,
                                Token = jwtGenerator.CreateToken(user),
                            };
                        }        
                        throw new Exception("Problem creating user");
                    }
                }
    }
}