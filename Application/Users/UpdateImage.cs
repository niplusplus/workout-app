using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Persistence;

namespace Application.Users
{
    public class UpdateImage
    {
        public class Command : IRequest
        {
            public IFormFile Image { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            private readonly UserManager<User> _userManager;
            private readonly IUserAccessor _userAccessor;
            public Handler(DataContext context, UserManager<User> userManager, IUserAccessor userAccessor)
            {
                this._userAccessor = userAccessor;
                this._userManager = userManager;
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(_userAccessor.GetCurrentUsername());
                if(user == null){
                    throw new RestException(HttpStatusCode.NotFound, new { user = "Not found" });
                }
                String fileName = user.UserName + "_";
                String path = Directory.GetParent(Directory.GetCurrentDirectory()) + "\\ClientApp\\public\\img\\UserProfileImages\\";
                if(request.Image != null){
                    fileName += request.Image.FileName;
                    path = Path.Combine(path, fileName);
                    using (var fs = new FileStream(path, FileMode.Create)){
                        await request.Image.CopyToAsync(fs);
                    }
                    user.ProfileImgPath = fileName;
                }
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}