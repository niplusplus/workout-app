using System;
using MediatR;
using Domain;
using System.Threading.Tasks;
using System.Threading;
using Persistence;
using Application.Errors;
using System.Net;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Application.Users
{
    public class Details
    {
        public class Query : IRequest<AppUser>
        {
            public String Id { get; set; }
        }

        public class Handler : IRequestHandler<Query, AppUser>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;
            public Handler(DataContext context, IMapper mapper)
            {
                this._mapper = mapper;
                this._context = context;

            }

            public async Task<AppUser> Handle(Query request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.FindAsync(request.Id);
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound, new { user = "Not found" });
                return _mapper.Map<User, AppUser>(user);
            }
        }
    }
}