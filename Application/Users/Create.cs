using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.Users
{
    public class Create
    {
        public class Command : IRequest
        {
            public String Id { get; set; }
            public String FullName { get; set; }
            public String NickName { get; set; }
            public String ProfileImgPath { get; set; }
            public bool isInstructor { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;

            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = new User{
                    Id = request.Id,
                    FullName = request.FullName,
                    NickName = request.NickName,
                    ProfileImgPath = "user.png",
                    isInstructor = request.isInstructor
                };

                this._context.Users.Add(user);
                var success = await _context.SaveChangesAsync() > 0;

                if(success) return Unit.Value;

                throw new Exception("Problem saving changes");
            }
        }
    }
}