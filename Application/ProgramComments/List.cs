using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.ProgramComments
{
    public class List
    {
        public class Query : IRequest<List<ProgramComment>> { }
        
                public class Handler : IRequestHandler<Query, List<ProgramComment>>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<List<ProgramComment>> Handle(Query request, CancellationToken cancellationToken)
                    {
                        return await _context.ProgramComments.OrderByDescending(x => x.Timestamp).ToListAsync();
                    }
                }
    }
}