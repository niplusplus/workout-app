using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.ProgramComments
{
    public class Create
    {
        public class Command : IRequest
                {
                    public String Name { get; set; }
                    public String Comment { get; set; }
                }
        
                public class Handler : IRequestHandler<Command>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
        
                    }
        
                    public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
                    {
                        var comment = new ProgramComment{
                            Name = request.Name,
                            Comment = request.Comment,
                            Timestamp = DateTime.Now
                        };
                        _context.ProgramComments.Add(comment);
                        var success = await _context.SaveChangesAsync() > 0;
        
                        if(success) return Unit.Value;
        
                        throw new Exception("Problem saving changes");
                    }
                }
    }
}