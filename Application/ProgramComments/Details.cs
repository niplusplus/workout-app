using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using MediatR;
using Persistence;

namespace Application.ProgramComments
{
    public class Details
    {
        public class Query : IRequest<ProgramComment> {
            public Guid Id { get; set; }
         }
        
                public class Handler : IRequestHandler<Query, ProgramComment>
                {
                    private readonly DataContext _context;
                    public Handler(DataContext context)
                    {
                        this._context = context;
                    }
        
                    public async Task<ProgramComment> Handle(Query request, CancellationToken cancellationToken)
                    {
                        var programComment = await _context.ProgramComments.FindAsync(request.Id);
                        if(programComment == null)
                            throw new RestException(HttpStatusCode.NotFound, new {programComment = "Not found"});
                        return programComment;
                    }
                }
    }
}