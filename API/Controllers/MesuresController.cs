using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Measures;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Persistence;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MeasuresController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MeasuresController(IMediator mediator)
        {
            this._mediator = mediator;


        }

        [HttpGet("listByUser/{id}")]
        public async Task<ActionResult<List<MeasureDTO>>> ListByUser(string id)
        {
            return await _mediator.Send(new ListByUser.Query {Id = id}); 
        }
        /*[HttpGet]
        public async Task<ActionResult<List<User>>> List()
        {
            return await _mediator.Send(new List.Query());
        }*/

        [HttpGet("user-measures")]
        public async Task<ActionResult<List<MeasureDTO>>> List()
        {
            return await _mediator.Send(new List.Query()); 
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }

        [HttpGet("last-measure")]
        public async Task<ActionResult<MeasureDTO>> Last()
        {
            return await _mediator.Send(new Last.Query()); 
        }

        [HttpPut("ProofImage")]
         public async Task<ActionResult<Unit>> EditPhoto([FromForm]UploadImage.Command command)
        {
            return await _mediator.Send(command);
        }       

        [HttpPut("progressImage")]
         public async Task<ActionResult<Unit>> EditProgressImagePhoto([FromForm]UploadProgressImage.Command command)
        {
            return await _mediator.Send(command);
        }  

        [HttpGet("userStepsImages/{id}")]
         public async Task<ActionResult<List<MeasurePhotoDTO>>> UserStepsImage(string id)
        {
            return await _mediator.Send(new UserStepsImage.Query {Id = id}); 
        }

        [HttpGet("userProgressImages/{id}")]
         public async Task<ActionResult<List<MeasurePhotoUserProgressDTO>>> UserProgressImages(string id)
        {
            return await _mediator.Send(new UserProgressImages.Query {Id = id}); 
        }
        [HttpDelete("userStepsImage/{id}")]
        public async Task<ActionResult<Unit>> DeleteStepsImage(Guid id){
            return await _mediator.Send(new DeleteStepsImage.Command{Id = id});
        }
        [HttpDelete("userProgressImage/{id}")]
        public async Task<ActionResult<Unit>> DeleteUserProgressImage(Guid id){
            return await _mediator.Send(new DeleteUserProgressImage.Command{Id = id});
        }
        /*[HttpPut("{id}")]
        public async Task<ActionResult<Unit>> Edit(Guid id, Edit.Command command){
            command.Id = id;
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }*/
    }
}
