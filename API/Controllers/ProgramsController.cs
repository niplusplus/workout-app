using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Programs;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramsController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ProgramsController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }

        [HttpPost("AddUser")]
        public async Task<ActionResult<Unit>> AddUserToProgram(AddUserToProgram.Command command){
            return await _mediator.Send(command);
        }
        [HttpGet("list")]
        public async Task<ActionResult<List<Domain.Program>>> List()
        {
            return await _mediator.Send(new List.Query());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<ProgramDTO>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query { Id = id });
        }
        [HttpGet("inProgress")]
        public async Task<ActionResult<ProgramDTO>> InProgress()
        {
            return await _mediator.Send(new InProgress.Query());
        }

        [HttpGet("next")]
        public async Task<ActionResult<ProgramDTO>> NextProgram()
        {
            return await _mediator.Send(new NextProgram.Query());
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send( new Delete.Command{Id = id} );
        }
    }
}