﻿using Application.ForumMessages;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForumMessagesController : ControllerBase
    {
        
        private readonly IMediator _mediator;

        public ForumMessagesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("list/{id}")]
        public async Task<ActionResult<List<ForumMessage>>> List(Guid id)
        {
            return await _mediator.Send(new List.Query { ForumThemeId = id });
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }
    }
}
