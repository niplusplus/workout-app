using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.GroupTasks;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GroupTasksController : ControllerBase
    {
        private readonly IMediator _mediator;

        public GroupTasksController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<ActionResult<List<GroupTask>>> List()
        {
            return await _mediator.Send(new List.Query());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GroupTask>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query{Id = id}); 
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }

    }
}