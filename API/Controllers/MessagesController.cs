using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Messages;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MessagesController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<ActionResult<List<MessageDTO>>> ListForAllByUser()
        {
            return await _mediator.Send(new ListForAllByUser.Query());
        }

        [HttpGet("{nickName}")]
        public async Task<ActionResult<List<MessageDTO>>> ListForPrivateByUsers(String nickName)
        {
            return await _mediator.Send(new ListForPrivateByUsers.Query{NickName = nickName});
        }
    }
}