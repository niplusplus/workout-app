using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Recipes;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RecipesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RecipesController(IMediator mediator)
        {
            this._mediator = mediator;
        }
        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }
        [HttpGet("list")]
        public async Task<ActionResult<List<Recipe>>> List()
        {
            return await _mediator.Send(new List.Query()); 
        }

        [HttpGet("list/{category}")]
        public async Task<ActionResult<List<Recipe>>> ListCategory(string category)
        {
            return await _mediator.Send(new ListCategory.Query {Category = category}); 
        }
        [HttpGet("list/state/{state}")]
        public async Task<ActionResult<List<Recipe>>> Liststate(String state)
        {
            return await _mediator.Send(new ListState.Query {State = state}); 
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Recipe>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query {Id = id}); 
        }

        [HttpPut("{state}/{id}")]
        public async Task<ActionResult<Unit>> ChangeState(String state, Guid id){
            return await _mediator.Send(new ChangeState.Command{Id = id, State = state});
        }
        [HttpPut("ImageUpload/{id}")]
        public async Task<ActionResult<Unit>> UploadImage(Guid id, [FromForm]UploadImage.Command command){
            command.Id = id;
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }
    }
}