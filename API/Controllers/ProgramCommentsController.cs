using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.ProgramComments;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramCommentsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProgramCommentsController(IMediator mediator)
        {
            this._mediator = mediator;
        }
        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }
        [HttpGet("list")]
        public async Task<ActionResult<List<ProgramComment>>> List()
        {
            return await _mediator.Send(new List.Query()); 
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<ProgramComment>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query {Id = id}); 
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }
    }
}