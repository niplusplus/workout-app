using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Appointments;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AppointmentsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Unit>> Schedule(Schedule.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet("list/{state}")]
        public async Task<ActionResult<List<Appointment>>> ListByState(String state)
        {
            return await _mediator.Send(new ListByState.Query { State = state });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Appointment>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query { Id = id });
        }
        [HttpGet("users")]
        public async Task<ActionResult<List<Appointment>>> ListByUser()
        {
            return await _mediator.Send(new ListByUser.Query {});
        }
        [HttpPost("approve")]
        public async Task<ActionResult<Unit>> Approve(Approve.Command command){
            return await _mediator.Send(command);
        }
        [HttpPost("deny")]
        public async Task<ActionResult<Unit>> Deny(Deny.Command command){
            return await _mediator.Send(command);
        }
        

    }
}