using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Ranglists;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RanglistController : ControllerBase
    {
        
        private readonly IMediator _mediator;

        public RanglistController(IMediator mediator)
        {
            this._mediator = mediator;
        }
        [HttpGet("steps-program")]
        public async Task<ActionResult<List<RanglistDTO>>> StepsProgram()
        {
            return await _mediator.Send(new StepsProgram.Query()); 
        }
        [HttpGet("weight-program")]
        public async Task<ActionResult<List<RanglistDTO>>> WeightProgram()
        {
            return await _mediator.Send(new WeightProgram.Query()); 
        }
        [HttpGet("waist-size-program")]
        public async Task<ActionResult<List<RanglistDTO>>> WaistSizeProgram()
        {
            return await _mediator.Send(new WaistSizeProgram.Query()); 
        }
        [HttpGet("km-program")]
        public async Task<ActionResult<List<RanglistDTO>>> KmProgram()
        {
            return await _mediator.Send(new KmProgram.Query()); 
        }
        [HttpGet("deeds-program")]
        public async Task<ActionResult<List<RanglistDTO>>> DeedsProgram()
        {
            return await _mediator.Send(new DeedsProgram.Query()); 
        }
    }
}