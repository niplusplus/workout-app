﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ForumThemes;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ForumThemeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ForumThemeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // GET: api/<ForumTheme>
        [HttpGet("list/{category}")]
        public async Task<ActionResult<List<ForumTheme>>> List(string category)
        {
            return await _mediator.Send(new List.Query { category = category });
        }

        // GET api/<ForumTheme>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ForumTheme>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query { Id = id });
        }

        // POST api/<ForumTheme>
        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }
        
    }
}
