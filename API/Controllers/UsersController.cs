using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Programs;
using Application.Users;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Persistence;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            this._mediator = mediator;


        }
        [HttpPost("login")]
        public async Task<ActionResult<AppUser>> Login(Login.Query query){
            return await _mediator.Send(query);
        }

        [HttpPost("register")]
        public async Task<ActionResult<AppUser>> Register(Register.Command command){
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<ActionResult<AppUser>> CurrentUser(){
            return await _mediator.Send(new CurrentUser.Query());
        }
        [HttpGet("list")]
        public async Task<ActionResult<List<AppUser>>> List()
        {
            return await _mediator.Send(new Application.Users.List.Query());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AppUser>> Details(String id)
        {
            return await _mediator.Send(new Application.Users.Details.Query{Id = id}); 
        }

        /*[HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }*/

        

        [HttpPut("{id}")]
        public async Task<ActionResult<Unit>> Edit(String id, Edit.Command command){
            command.Id = id;
            return await _mediator.Send(command);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(String id){
            return await _mediator.Send(new Application.Users.Delete.Command{Id = id});
        }
        [HttpGet("OnProgram")]
        public async Task<ActionResult<UserInProgramDTO>> OnProgram()
        {
            return await _mediator.Send(new OnProgram.Query()); 
        }
        [HttpPut("profileImage")]
         public async Task<ActionResult<Unit>> EditPhoto([FromForm]Application.Users.UpdateImage.Command command)
        {
            return await _mediator.Send(command);
        }
    }
}
