using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Application.Surveys;
using System;
using System.Collections.Generic;
using Domain;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SurveysController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SurveysController(IMediator mediator)
        {
            this._mediator = mediator;
        }
        [HttpPost]
        public async Task<ActionResult<Unit>> Create(Create.Command command){
            return await _mediator.Send(command);
        }
        [HttpGet("list")]
        public async Task<ActionResult<List<Survey>>> List()
        {
            return await _mediator.Send(new List.Query()); 
        }

        [HttpGet("list/{state}")]
        public async Task<ActionResult<List<Survey>>> ListByState(String state)
        {
            return await _mediator.Send(new ListByState.Query {State = state}); 
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Survey>> Details(Guid id)
        {
            return await _mediator.Send(new Details.Query {Id = id}); 
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Unit>> Delete(Guid id){
            return await _mediator.Send(new Delete.Command{Id = id});
        }
        [HttpPut("{state}/{id}")]
        public async Task<ActionResult<Unit>> ChangeState(String state, Guid id){
            return await _mediator.Send(new ChangeState.Command{Id = id, State = state});
        }
    }
}