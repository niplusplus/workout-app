using System;

namespace Domain
{
    public class ForumMessage
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
        public string AuthorName { get; set; }
        public DateTime CreateDate { get; set; }
        public Guid ForumThemeId { get; set; }
        public ForumTheme ForumTheme { get; set; }
    }
}