using System;
using System.Collections.Generic;

namespace Domain
{
    public class Program
    {
        public Guid Id { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public ICollection<UserInProgram> UsersInProgram { get; set; }
    }
}