using System;

namespace Domain
{
    public class UserInProgram
    {
        public String UserId { get; set; }
        public User User {get; set; }
        public Guid ProgramId { get; set; }
        public Program Program { get; set; }
        public double WeightAtStart { get; set; }
        public double WeistSizeAtStart { get; set; }
        public double WantedWeight { get; set; }
        public int AvgStepsPerDay { get; set; }
        public int AllStepsOnProgram { get; set; }
        public double LostCm { get; set; }
        public double LostWeight { get; set; }
    }
}