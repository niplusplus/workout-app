using System;

namespace Domain
{
    public class GroupTask
    {
        public Guid Id { get; set; }
        public string Task { get; set; }
        public DateTime Timestamp { get; set; }
    }
}