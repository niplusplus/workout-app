using System;
using System.Collections.Generic;

namespace Domain
{
    public class Measure
    {
        public Guid Id { get; set; }
        public int NumberOfSteps { get; set; }
        public double Weight { get; set; }
        public double WaistSize { get; set; }
        public string GoodDeed1 { get; set; }
        public string GoodDeed2 { get; set; }
        public string GoodDeed3 { get; set; }
        public string GoodDeed4 { get; set; }
        public string GoodDeed5 { get; set; }
        public int DeedCounter { get; set; }
        public string ImagePath { get; set; }
        public string UserProgressImagePath { get; set; }
        public User User { get; set; }
        public String UserId { get; set; }
        public DateTime Created { get; set; }
    }
}