using System;

namespace Domain
{
    public class ProgramComment
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public String Comment { get; set; }
        public DateTime Timestamp { get; set; }
    }
}