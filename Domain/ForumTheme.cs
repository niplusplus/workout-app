using System;
using System.Collections.Generic;

namespace Domain
{
    public class ForumTheme
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string OwnerName { get; set; }
        public DateTime CreateDate { get; set; }
        public int NumberOfMessages { get; set; }
        public string Category { get; set; }
        public ICollection<ForumMessage> ForumMessages { get; set; } 
    }
}