using System;
using System.Collections.Generic;

namespace Domain
{
    public class Chat
    {
        public Guid Id { get; set; }
        public ICollection<Message> Messages { get; set; }
        public String ChatType { get; set; }
    }
}