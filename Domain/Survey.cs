using System;

namespace Domain
{
    public class Survey
    {
        public Guid Id { get; set; }
        public String Fullname { get; set; }
        public String City {get; set; }
        public String Address {get; set; }
        public String Birth { get; set; }
        public String Number { get; set; }
        public String Motive { get; set;}
        public String Obligations { get; set; }
        public String Sweets { get; set; }
        public String Diet { get; set; }
        public String Difficulty { get; set; }
        public String Medicines { get; set; }
        public String Opinion { get; set; }
        public String Mistakes { get; set; }
        public String Weight { get; set; }
        public String Height { get; set; }
        public String MagicNumber { get; set; }
        public String Training { get; set; }
        public String Measurement { get; set; }
        public String Questions { get; set; }
        public String State { get; set; }
    }
}