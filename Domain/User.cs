﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Domain
{
    public class User : IdentityUser
    {
        public String FullName { get; set; }
        public String NickName { get; set; }
        public String ProfileImgPath { get; set; }
        public double Weight { get; set; }
        public double WaistSize { get; set; }
        public bool isInstructor { get; set; }
        public ICollection<Measure> Measures { get; set; }
        public ICollection<UserInProgram> UserInPrograms { get; set; }
    }
}
