using System;
using System.Collections.Generic;

namespace Domain
{
    public class Message
    {
        public Guid Id { get; set; }
        public String Text { get; set; }
        public User Sender { get; set; }
        public String SenderId { get; set; }
        public User Reciver { get; set; }
        public String ReciverId { get; set; }
        public String Type { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}