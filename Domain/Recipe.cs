using System;

namespace Domain
{
    public class Recipe
    {
        public Guid Id { get; set; }
        public String Title { get; set; }
        public String ShortDescription { get; set; }
        public String Ingredients { get; set; }
        public String Description { get; set; }
        public String Category { get; set; }
        public String State { get; set; }
        public String ImagePath { get; set; }
        public String TimeToPrepare { get; set; }
    }
}

