using System;

namespace Domain
{
    public class Appointment
    {
        public Guid Id { get; set; }
        public String Time { get; set; }
        public String State { get; set; }
        public String UserId { get; set; }
        public String UserName { get; set; }  
        public String FullName { get; set; }
        public String Email { get; set; }
    }
}