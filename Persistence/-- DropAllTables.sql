-- SQLite
DROP TABLE Appointments
DROP TABLE AspNetRoleClaims
DROP TABLE AspNetRoles
DROP TABLE AspNetUserClaims
DROP TABLE AspNetUserLogins
DROP TABLE AspNetUserRoles
DROP TABLE AspNetUserTokens
DROP TABLE AspNetUsers
DROP TABLE ForumMessages
DROP TABLE ForumThemes
DROP TABLE GroupTasks
DROP TABLE Measures
DROP TABLE Programs
DROP TABLE Recipes
DROP TABLE UsersInPrograms
DROP TABLE __EFMigrationsHistory