﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class SurveyAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Surveys",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Fullname = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Birth = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    Motive = table.Column<string>(nullable: true),
                    Obligations = table.Column<string>(nullable: true),
                    Sweets = table.Column<string>(nullable: true),
                    Diet = table.Column<string>(nullable: true),
                    Difficulty = table.Column<string>(nullable: true),
                    Medicines = table.Column<string>(nullable: true),
                    Opinion = table.Column<string>(nullable: true),
                    Mistakes = table.Column<string>(nullable: true),
                    Weight = table.Column<string>(nullable: true),
                    Height = table.Column<string>(nullable: true),
                    MagicNumber = table.Column<string>(nullable: true),
                    Training = table.Column<string>(nullable: true),
                    Measurement = table.Column<string>(nullable: true),
                    Questions = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surveys", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Surveys");
        }
    }
}
