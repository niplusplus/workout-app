﻿using System;
using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Persistence
{
    public class DataContext : IdentityDbContext<User>
    {
        public DataContext(DbContextOptions options) : base(options)
        {            
        }

        public DbSet<Measure> Measures { get; set; }
        public DbSet<GroupTask> GroupTasks { get; set; }
        public DbSet<ForumTheme> ForumThemes { get; set; }
        public DbSet<ForumMessage> ForumMessages { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<UserInProgram> UsersInPrograms { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<ProgramComment> ProgramComments { get; set; }
        public DbSet<Message> Messages { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Measure>(x => x.HasKey(um => 
            new {um.UserId, um.Id}));

            builder.Entity<UserInProgram>(x => x.HasKey(up => 
            new { up.UserId, up.ProgramId}));

            builder.Entity<Measure>()
                .HasOne(u => u.User)
                .WithMany(m => m.Measures)
                .HasForeignKey(u => u.UserId);

            builder.Entity<ForumMessage>()
                .HasOne(u => u.ForumTheme)
                .WithMany(a => a.ForumMessages)
                .HasForeignKey(u => u.ForumThemeId);

            builder.Entity<UserInProgram>()
                .HasOne(u => u.User)
                .WithMany(up => up.UserInPrograms)
                .HasForeignKey(u => u.UserId);
            
            builder.Entity<UserInProgram>()
            .HasOne(p => p.Program)
            .WithMany(pu => pu.UsersInProgram)
            .HasForeignKey(p => p.ProgramId);

        }
    }
}
