using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain;
using Microsoft.AspNetCore.Identity;

namespace Persistence
{
    public class Seed
    {
        public static async Task SeedData(DataContext context, UserManager<User> userManager){

            if(!context.Users.Any()){
                 var users = new List<User>{
                     new User{
                        FullName = "Nikola Kalinic",
                        UserName = "Kalina",
                        ProfileImgPath = "asda/asdasd/dasdas.jpg",
                        Email = "kalina@kalina.com",
                        isInstructor = false,
                        WaistSize = 60,
                        Weight = 90,
                     },
                     new User{
                        FullName = "Nikola Malinic",
                        UserName = "Malina",
                        ProfileImgPath = "asda/asdasd/dasdas.jpg",
                        Email = "malina@malina.com",
                        isInstructor = false,
                        WaistSize = 58,
                        Weight = 87,
                     },
                     new User{
                        FullName = "Djordje Djordjevic",
                        UserName = "Djordjino",
                        ProfileImgPath = "asda/asdasd/dasdas.jpg",
                        Email = "Admin@Admin.com",
                        isInstructor = true,
                        WaistSize = 86,
                        Weight = 130,
                     }
                 };
                 foreach (var user in users){
                     await userManager.CreateAsync(user, "Pa$$w0rd");
                 }
            }
        }
    }
}